
<!DOCTYPE html>
<html class="no-js" lang="en">
   <!-- Mirrored from demo.artureanec.com/html/crypterium/index_8.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 10 Mar 2022 08:28:05 GMT -->
@php
use App\Enums\UserRoles;
$style = (lang_dir() == 'rtl') ? 'apps.rtl' : 'apps';
@endphp
   <head>
      <title>Mining Storm</title>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
      <meta name="viewport" content="user-scalable=no, width=device-width, height=device-height, initial-scale=1, maximum-scale=1, minimum-scale=1, minimal-ui" />
      <meta name="theme-color" content="#3F6EBF" />
      <meta name="msapplication-navbutton-color" content="#3F6EBF" />
      <meta name="apple-mobile-web-app-status-bar-style" content="#3F6EBF" />
      <!-- Favicons
         ================================================== -->
      <link rel="shortcut icon" href="/favicon.ico">
      <!-- CSS
         ================================================== -->
      <link rel="stylesheet" href="/landing/css/style.min.css" type="text/css">
      <!-- Load google font
         ================================================== -->
      <script type="text/javascript">
         WebFontConfig = {
             google: { families: [ 'Open+Sans:300,400,500','Lato:900', 'Poppins:400', 'Catamaran:300,400,500,600,700'] }
         };
         (function() {
             var wf = document.createElement('script');
             wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
             '://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js';
             wf.type = 'text/javascript';
             wf.async = 'true';
             var s = document.getElementsByTagName('script')[0];
             s.parentNode.insertBefore(wf, s);
         })();
      </script>
      <!-- Load other scripts
         ================================================== -->
      <script type="text/javascript">
         var _html = document.documentElement,
             isTouch = (('ontouchstart' in _html) || (navigator.msMaxTouchPoints > 0) || (navigator.maxTouchPoints));

         _html.className = _html.className.replace("no-js","js");

         isTouch ? _html.classList.add("touch") : _html.classList.add("no-touch");
      </script>
      <script type="text/javascript" src="/landing/js/device.min.js"></script>
   </head>
   <body>
      <!-- start header -->
      <header id="top-bar" class="top-bar--light">
         <div id="top-bar__inner">
            <a id="top-bar__logo" class="site-logo" href="{{url('/')}}">
            <img class="img-responsive" width="197" height="90" src="/landing/img/white_logo.png" alt="demo" />
            <img class="img-responsive" width="197" height="90" src="/landing/img/white_logo.png" alt="demo" />
            </a>
            <a id="top-bar__navigation-toggler" href="javascript:void(0);"><span></span></a>
            <div id="top-bar__navigation-wrap">
               <div>
                  <nav id="top-bar__navigation" class="navigation" role="navigation">
                     <ul>
                        <li class="active">
                           <a href="{{url('/')}}"><span>Home</span></a>
                        </li>

                     </ul>
                  </nav>
                  <br class="hide--lg">
                  <ul id="top-bar__subnavigation">
                     @guest
                     <li><a class="custom-btn custom-btn--small custom-btn--style-3" href="{{ route('auth.login.form') }}">Login</a></li>
                     @else
                     <li><a class="custom-btn custom-btn--small custom-btn--style-3" href="{{ (auth()->user()->role==UserRoles::USER) ? route('dashboard') : route('admin.dashboard') }}">Dashboard</a></li>
                     @endguest
                     @if (!auth()->check() && gss('signup_allow', 'enable') == 'enable')
                     <li><a  href="{{ route('auth.register.form') }}">Sign Up</a></li>
                     @endif
                  </ul>
               </div>
            </div>
         </div>
      </header>
      <!-- end header -->
      <!-- start start screen -->
      <div id="start-screen" class="start-screen--light start-screen--style-8">
         <div class="start-screen__static-bg" style="background-image: url(/landing/img/bg_18.png);background-position: center bottom;">
            <div class="start-screen__content">
               <div class="start-screen__content__inner">
                  <div class="grid grid--container">
                     <div class="row row--xs-center">
                        <div class="col col--md-7">
                           <h1 class="__title">Earn Daily Mining</h1>
                           <div class="row  col-MB-40">
                              <div class="col col--lg-11 col--xl-9">
                                 <div class="col-MB-25">
                                    Mining Storm allows anyone to utilise the power of our very powerfull mining farms and start making money instantly
                                 </div>
                                 <h3 class="__title">Earn anything from $200 every week mining</h3>
                            <div class="row row--xs-center">
                                    <div class="col col--xs-auto">
<a class="custom-btn custom-btn--medium custom-btn--style-5" style="margin-top: 20px;" href="{{url('dashboard')}}">Start Mining</a>
                                    </div>

                                 </div>
                              </div>
                           </div>
                           <div class="facts">
                              <div class="__inner">
                                 <div class="row">
                                    <div class="col col--xs-auto col--lg-4">
                                       <div class="__item text--sm-left">
                                          Mine
                                          <span class="num js-count" data-from="0" data-to="15" data-decimals="0"></span><br>
                                          Cryptocurrencies
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- end start screen -->
      <!-- start main -->
      <main role="main">
         <!-- start section -->
         <section class="section">
            <div class="grid grid--container">
               <div class="row">
                  <div class="col col--md-5 col--lg-4">
                     <div data-aos="fade-right">
                        <div class="section-heading  col-MB-40">
                           <h5 class="__subtitle">About Mining Storm</h5>
                           <h2 class="__title">Best Cloud Mining Farm</h2>
                        </div>

                     </div>
                  </div>
                  <div class="col hide--md col-MB-10"></div>
                  <div class="col col--md-7 col--lg-8">
                     <p data-aos="fade-up">
                       Mining Storm is one of the best crypto mining farms in the world and it allows any person with or without mining experience to start mining a variety of cryptocurrencies and earn consistent profits everyday.
                     </p>
                     <p data-aos="fade-up">
                        Mining Storm allows you to customise your mining style and the way you woyld want your profits to come out depending with your mining budget.
                     </p>
                     <p data-aos="fade-up">
                        Reffering other users is very beneficial in that you earn a percentage of their mining budget directly into your acccount.
                     </p>
                  </div>
               </div>
            </div>
         </section>
         <!-- end section -->
         <!-- start section -->
         <section class="section section--no-pt">
            <div class="grid grid--container">
               <div class="section-heading section-heading--center  col-MB-60">
                  <h5 class="__subtitle">Plans</h5>
                  <h2 class="__title">Our Mining Plans</h2>
               </div>
               <div class="pricing-tab">
                  <div class="tab-container">
                     <nav class="tab-nav  text--center">
                        <a href="javascript:void(0);">
                        <img class="lazy" width="25" height="25" src="/landing/img/blank.gif" data-src="/landing/img/ico/ico_btc2.png" alt="demo" />
                        <strong>Plans</strong>
                        </a>

                     </nav>
                     <div class="tab-content">
                        <div class="tab-content__item">
                           <!-- start pricing table -->
                           <div class="pricing-table pricing-table--style-4">
                              <div class="__inner">
                                 <div class="row">

                                    <!-- start item -->
                                    <div class="col col--sm-6 col--lg-3 col--sm-flex">
                                       <div class="__item __item--color-2" data-aos="flip-left" data-aos-easing="ease-out-cubic" data-aos-delay="300">
                                          <div>
                                             <h4 class="__title">Standard Plan</h4>
                                             <p class="__price">3.5%<sub>/per day</sub></p>
                                          </div>
                                          <ul class="__desc-list">
                                             <li>
                                                <strong>$20-$5000</strong> <br>
                                                <span class="__note">Weekly Withdrawals</span>
                                             </li>
                                             <li>
                                                Total Return <br>
                                                <strong>310%</strong>
                                             </li>
                                             <li>
                                                Contract Duration <br>
                                                <strong>2 Months</strong>
                                             </li>
                                          </ul>
                                          <a class="custom-btn custom-btn--medium custom-btn--style-2 wide" href="{{url('dashboard')}}">Get Started</a>
                                       </div>
                                    </div>
                                    <!-- end item -->
                                    <!-- start item -->
                                    <div class="col col--sm-6 col--lg-3 col--sm-flex">
                                       <div class="__item __item--color-3 __item--active" data-aos="flip-left" data-aos-easing="ease-out-cubic" data-aos-delay="450">
                                          <span class="__label">Most Invested</span>
                                          <div>
                                             <h4 class="__title">Business Plan</h4>
                                             <p class="__price">4%<sub>/per day</sub></p>
                                          </div>
                                          <ul class="__desc-list">
                                             <li>
                                                <strong>$50-$10000</strong> <br>
                                                <span class="__note">Withdraw every 14 Days</span>
                                             </li>
                                             <li>
                                                <br>
                                                 Total Return <br>
                                                <strong>340%</strong>
                                             </li>
                                             <li>
                                                Contract Duration <br>
                                                <strong>2 Months</strong>
                                             </li>
                                          </ul>
                                          <a class="custom-btn custom-btn--medium custom-btn--style-2 wide" href="{{url('dashboard')}}">Get Started</a>
                                       </div>
                                    </div>
                                    <!-- end item -->
                                    <!-- start item -->
                                    <div class="col col--sm-6 col--lg-3 col--sm-flex">
                                       <div class="__item __item--color-4" data-aos="flip-left" data-aos-easing="ease-out-cubic" data-aos-delay="600">
                                          <div>
                                             <h4 class="__title">Proffesional Plan</h4>
                                             <p class="__price">5.5%<sub>/per day</sub></p>
                                          </div>
                                          <ul class="__desc-list">
                                             <li>
                                                <strong>$100-$100k</strong> <br>
                                                <span class="__note">Withdraw Monthly</span>
                                             </li>
                                             <li>
                                                Total Return<br>
                                                <strong>430%</strong>
                                             </li>
                                             <li>
                                                Contract Duration <br>
                                                <strong>2 Months</strong>
                                             </li>
                                          </ul>
                                          <a class="custom-btn custom-btn--medium custom-btn--style-2 wide" href="{{url('dashboard')}}">Get Started</a>
                                       </div>
                                    </div>
                                    <!-- end item -->
                                 </div>
                              </div>
                           </div>
                           <!-- end pricing table -->
                        </div>
                        <div class="tab-content__item">
                           <!-- start pricing table -->
                           <div class="pricing-table pricing-table--style-4">
                              <div class="__inner">
                                 <div class="row">
                                    <!-- start item -->
                                    <div class="col col--sm-6 col--lg-3 col--sm-flex">
                                       <div class="__item __item--color-2" data-aos="flip-left" data-aos-easing="ease-out-cubic" data-aos-delay="300">
                                          <div>
                                             <h4 class="__title">Standart</h4>
                                             <p class="__price">9.99$<sub>/mo</sub></p>
                                          </div>
                                          <ul class="__desc-list">
                                             <li>
                                                <strong>0.5 KH/s</strong> <br>
                                                <span class="__note">Free setup</span>
                                             </li>
                                             <li>
                                                Maintenance Fees per GH/s/day <br>
                                                <strong>$ 0.299</strong>
                                             </li>
                                             <li>
                                                Contract Duration <br>
                                                <strong>24 Months</strong>
                                             </li>
                                          </ul>
                                          <a class="custom-btn custom-btn--medium custom-btn--style-2 wide" href="#">Get Started</a>
                                       </div>
                                    </div>
                                    <!-- end item -->
                                    <!-- start item -->
                                    <div class="col col--sm-6 col--lg-3 col--sm-flex">
                                       <div class="__item __item--color-3 __item--active" data-aos="flip-left" data-aos-easing="ease-out-cubic" data-aos-delay="450">
                                          <span class="__label">Best match</span>
                                          <div>
                                             <h4 class="__title">Optima</h4>
                                             <p class="__price">29.99$<sub>/mo</sub></p>
                                          </div>
                                          <ul class="__desc-list">
                                             <li>
                                                <strong>0.5 KH/s</strong> <br>
                                                <span class="__note">Free setup</span>
                                             </li>
                                             <li>
                                                Maintenance Fees per GH/s/day <br>
                                                <strong>$ 0.299</strong>
                                             </li>
                                             <li>
                                                Contract Duration <br>
                                                <strong>24 Months</strong>
                                             </li>
                                          </ul>
                                          <a class="custom-btn custom-btn--medium custom-btn--style-2 wide" href="#">Get Started</a>
                                       </div>
                                    </div>
                                    <!-- end item -->
                                    <!-- start item -->
                                    <div class="col col--sm-6 col--lg-3 col--sm-flex">
                                       <div class="__item __item--color-4" data-aos="flip-left" data-aos-easing="ease-out-cubic" data-aos-delay="600">
                                          <div>
                                             <h4 class="__title">Premium</h4>
                                             <p class="__price">199.99$<sub>/mo</sub></p>
                                          </div>
                                          <ul class="__desc-list">
                                             <li>
                                                <strong>0.5 KH/s</strong> <br>
                                                <span class="__note">Free setup</span>
                                             </li>
                                             <li>
                                                Maintenance Fees per GH/s/day <br>
                                                <strong>$ 0.299</strong>
                                             </li>
                                             <li>
                                                Contract Duration <br>
                                                <strong>24 Months</strong>
                                             </li>
                                          </ul>
                                          <a class="custom-btn custom-btn--medium custom-btn--style-2 wide" href="#">Get Started</a>
                                       </div>
                                    </div>
                                    <!-- end item -->
                                    <!-- start item -->
                                    <div class="col col--sm-6 col--lg-3 col--sm-flex">
                                       <div class="__item __item--color-1" data-aos="flip-left" data-aos-easing="ease-out-cubic" data-aos-delay="150">
                                          <div>
                                             <h4 class="__title">Basic</h4>
                                             <p class="__price">Free</p>
                                          </div>
                                          <ul class="__desc-list">
                                             <li>
                                                <strong>0.5 KH/s</strong> <br>
                                                <span class="__note">Free setup</span>
                                             </li>
                                             <li>
                                                Maintenance Fees per GH/s/day <br>
                                                <strong>$ 0.299</strong>
                                             </li>
                                             <li>
                                                Contract Duration <br>
                                                <strong>24 Months</strong>
                                             </li>
                                          </ul>
                                          <a class="custom-btn custom-btn--medium custom-btn--style-2 wide" href="#">Get Started</a>
                                       </div>
                                    </div>
                                    <!-- end item -->
                                 </div>
                              </div>
                           </div>
                           <!-- end pricing table -->
                        </div>
                        <div class="tab-content__item">
                           <!-- start pricing table -->
                           <div class="pricing-table pricing-table--style-4">
                              <div class="__inner">
                                 <div class="row">
                                    <!-- start item -->
                                    <div class="col col--sm-6 col--lg-3 col--sm-flex">
                                       <div class="__item __item--color-3 __item--active" data-aos="flip-left" data-aos-easing="ease-out-cubic" data-aos-delay="450">
                                          <span class="__label">Best match</span>
                                          <div>
                                             <h4 class="__title">Optima</h4>
                                             <p class="__price">29.99$<sub>/mo</sub></p>
                                          </div>
                                          <ul class="__desc-list">
                                             <li>
                                                <strong>0.5 KH/s</strong> <br>
                                                <span class="__note">Free setup</span>
                                             </li>
                                             <li>
                                                Maintenance Fees per GH/s/day <br>
                                                <strong>$ 0.299</strong>
                                             </li>
                                             <li>
                                                Contract Duration <br>
                                                <strong>24 Months</strong>
                                             </li>
                                          </ul>
                                          <a class="custom-btn custom-btn--medium custom-btn--style-2 wide" href="#">Get Started</a>
                                       </div>
                                    </div>
                                    <!-- end item -->
                                    <!-- start item -->
                                    <div class="col col--sm-6 col--lg-3 col--sm-flex">
                                       <div class="__item __item--color-4" data-aos="flip-left" data-aos-easing="ease-out-cubic" data-aos-delay="600">
                                          <div>
                                             <h4 class="__title">Premium</h4>
                                             <p class="__price">199.99$<sub>/mo</sub></p>
                                          </div>
                                          <ul class="__desc-list">
                                             <li>
                                                <strong>0.5 KH/s</strong> <br>
                                                <span class="__note">Free setup</span>
                                             </li>
                                             <li>
                                                Maintenance Fees per GH/s/day <br>
                                                <strong>$ 0.299</strong>
                                             </li>
                                             <li>
                                                Contract Duration <br>
                                                <strong>24 Months</strong>
                                             </li>
                                          </ul>
                                          <a class="custom-btn custom-btn--medium custom-btn--style-2 wide" href="#">Get Started</a>
                                       </div>
                                    </div>
                                    <!-- end item -->
                                    <!-- start item -->
                                    <div class="col col--sm-6 col--lg-3 col--sm-flex">
                                       <div class="__item __item--color-1" data-aos="flip-left" data-aos-easing="ease-out-cubic" data-aos-delay="150">
                                          <div>
                                             <h4 class="__title">Basic</h4>
                                             <p class="__price">Free</p>
                                          </div>
                                          <ul class="__desc-list">
                                             <li>
                                                <strong>0.5 KH/s</strong> <br>
                                                <span class="__note">Free setup</span>
                                             </li>
                                             <li>
                                                Maintenance Fees per GH/s/day <br>
                                                <strong>$ 0.299</strong>
                                             </li>
                                             <li>
                                                Contract Duration <br>
                                                <strong>24 Months</strong>
                                             </li>
                                          </ul>
                                          <a class="custom-btn custom-btn--medium custom-btn--style-2 wide" href="#">Get Started</a>
                                       </div>
                                    </div>
                                    <!-- end item -->
                                    <!-- start item -->
                                    <div class="col col--sm-6 col--lg-3 col--sm-flex">
                                       <div class="__item __item--color-2" data-aos="flip-left" data-aos-easing="ease-out-cubic" data-aos-delay="300">
                                          <div>
                                             <h4 class="__title">Standart</h4>
                                             <p class="__price">9.99$<sub>/mo</sub></p>
                                          </div>
                                          <ul class="__desc-list">
                                             <li>
                                                <strong>0.5 KH/s</strong> <br>
                                                <span class="__note">Free setup</span>
                                             </li>
                                             <li>
                                                Maintenance Fees per GH/s/day <br>
                                                <strong>$ 0.299</strong>
                                             </li>
                                             <li>
                                                Contract Duration <br>
                                                <strong>24 Months</strong>
                                             </li>
                                          </ul>
                                          <a class="custom-btn custom-btn--medium custom-btn--style-2 wide" href="#">Get Started</a>
                                       </div>
                                    </div>
                                    <!-- end item -->
                                 </div>
                              </div>
                           </div>
                           <!-- end pricing table -->
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <!-- end section -->

         <!-- start section -->
         <section class="section">
            <div class="grid grid--container">
               <div class="row row--md-center">
                  <div class="col col--md-5 col--lg-6">
                     <img class="img-responsive" src="/landing/img/img_10.png" alt="demo" />
                  </div>
                  <div class="col hide--md col-MB-40"></div>
                  <div class="col col--md-7 col--lg-6">
                     <div data-aos="fade-left" data-aos-delay="150">
                        <div class="section-heading  col-MB-30">
                           <h5 class="__subtitle">The Process</h5>
                           <h2 class="__title">Start Mining with Three Simple Steps</h2>
                        </div>
                        <p class="col-MB-35">
                           Mining Storm makes it very simple to mine any crypto from the comfort of your home.
                        </p>
                        <ul class="special-list">
                           <li class="list__item list__item--1">
                              <div class="b-table">
                                 <div class="cell v-middle">
                                    <i class="ico fontello-lock"></i>
                                 </div>
                                 <div class="cell v-middle">
                                    <h6>Register Account</h6>
                                 </div>
                              </div>
                           </li>
                           <li class="list__item list__item--2">
                              <div class="b-table">
                                 <div class="cell v-middle">
                                    <i class="ico fontello-plane"></i>
                                 </div>
                                 <div class="cell v-middle">
                                    <h6>Choose your plan</h6>
                                 </div>
                              </div>
                           </li>
                           <li class="list__item list__item--3">
                              <div class="b-table">
                                 <div class="cell v-middle">
                                    <i class="ico fontello-wallet"></i>
                                 </div>
                                 <div class="cell v-middle">
                                    <h6>Receive Currency</h6>
                                 </div>
                              </div>
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
               <div class="col col-MB-25">&nbsp;</div>
               <div class="col col-MB-25">&nbsp;</div>
               <div class="row row--md-center row--md-reverse">
                  <div class="col col--md-5 col--lg-6">
                     <img class="img-responsive" src="/landing/img/img_11.png" alt="demo" />
                  </div>
                  <div class="col hide--md col-MB-40"></div>
                  <div class="col col--md-7 col--lg-6">
                     <div class="content-grid" style="margin-left: auto;max-width: 570px;">
                        <div data-aos="fade-right" data-aos-delay="150">
                           <div class="section-heading  col-MB-30">
                              <h2 class="__title">Take advantage of our modern and powerfull equipment</h2>
                           </div>
                           <p class="col-MB-35">
                              Mining storm boasts with high end mining equpment, that is tailormade for each and every mining task.
                           </p>
                           <p>
                              <a class="custom-btn custom-btn--medium custom-btn--style-1" href="{{url('dashboard')}}">Start Now</a>
                           </p>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <!-- end section -->
         <!-- start section -->
         <section class="section section--dark-bg section--custom-01">
            <style type="text/css">
               @media only screen and (min-width: 768px)
               {
               .section--custom-01 .feature--style-1
               {
               width: 65vw;
               }
               .section--custom-01 .feature--style-1 .slick-dots
               {
               text-align: left;
               }
               }
               @media only screen and (min-width: 992px)
               {
               .section--custom-01 .feature--style-1
               {
               margin-left: 0 !important;
               margin-right: 0 !important;
               }
               }
            </style>
            <div class="grid grid--container">
               <div class="row row--xs-center">
                  <div class="col col--md-6 col--lg-5 col--xl-4">
                     <div class="col-MB-35 col-md-MB-0" data-aos="fade-up" data-aos-easing="ease-out-cubic">
                        <div class="section-heading section-heading--white  col-MB-30">
                           <h5 class="__subtitle">Mining Storm</h5>
                           <h2 class="__title">Why choose Miningstorm</h2>
                        </div>
                        <p>

                        </p>
                     </div>
                  </div>
                  <div class="col col--md-6 col--lg-7 col--xl-7 col--xl-offset-1">
                     <!-- start feature -->
                     <div class="feature feature--style-1 feature--slider  text--center text--sm-left"
                        data-slick='{
                        "autoplay": true,
                        "arrows": false,
                        "dots": true,
                        "speed": 1000,
                        "responsive": [
                        {
                        "breakpoint":560,
                        "settings":{
                        "centerMode": true,
                        "centerPadding": "20%"
                        }
                        },
                        {
                        "breakpoint":767,
                        "settings":{
                        "centerMode": false,
                        "slidesToShow": 2
                        }
                        },
                        {
                        "breakpoint":1500,
                        "settings":{
                        "slidesToShow": 3
                        }
                        }
                        ]}'>
                        <!-- start item -->
                        <div class="__item  __item--first">
                           <div class="__content">
                              <i class="__ico">
                              <img class="img-responsive" src="/landing/img/feature_img/1.png" width="34" height="60" alt="demo" />
                              </i>
                              <h3 class="__title">Mobile App</h3>
                              <p> Our Mining panel works on your Android or iPhone in addition to your web browser.</p>
                           </div>
                        </div>
                        <!-- end item -->
                        <!-- start item -->
                        <div class="__item  __item--second">
                           <div class="__content">
                              <i class="__ico">
                              <img class="img-responsive" src="/landing/img/feature_img/2.png" width="46" height="60" alt="demo" />
                              </i>
                              <h3 class="__title">Protection</h3>
                              <p>Digital currency stored on our servers is covered by Violet insurance.</p>
                           </div>
                        </div>
                        <!-- end item -->
                        <!-- start item -->
                        <div class="__item  __item--third">
                           <div class="__content">
                              <i class="__ico">
                              <img class="img-responsive" src="/landing/img/feature_img/3.png" width="46" height="60" alt="demo" />
                              </i>
                              <h3 class="__title">Secure Storage</h3>
                              <p>We store the vast majority of the digital assets in secure offline storage.</p>
                           </div>
                        </div>
                        <!-- end item -->
                        <!-- start item -->
                        <div class="__item  __item--first">
                           <div class="__content">
                              <i class="__ico">
                              <img class="img-responsive" src="/landing/img/feature_img/1.png" width="34" height="60" alt="demo" />
                              </i>
                              <h3 class="__title">Mobile App</h3>
                              <p> Our Mining panel works on your Android or iPhone in addition to your web browser.</p>
                           </div>
                        </div>
                        <!-- end item -->
                        <!-- start item -->
                        <div class="__item  __item--second">
                           <div class="__content">
                              <i class="__ico">
                              <img class="img-responsive" src="/landing/img/feature_img/2.png" width="46" height="60" alt="demo" />
                              </i>
                              <h3 class="__title">Protection</h3>
                              <p>Digital currency stored on our servers is covered by Violet insurance.</p>
                           </div>
                        </div>
                        <!-- end item -->
                        <!-- start item -->
                        <div class="__item  __item--third">
                           <div class="__content">
                              <i class="__ico">
                              <img class="img-responsive" src="/landing/img/feature_img/3.png" width="46" height="60" alt="demo" />
                              </i>
                              <h3 class="__title">Secure Storage</h3>
                              <p>We store the vast majority of the digital assets in secure offline storage.</p>
                           </div>
                        </div>
                        <!-- end item -->
                     </div>
                     <!-- end feature -->
                  </div>
               </div>
            </div>
         </section><br><br>
         <!-- end section -->


         <!-- start section -->
         <section class="section section--no-pt section--no-pb">
            <div class="grid grid--container">
               <div class="row">
                  <div class="col col--md-5 col--lg-4">
                     <div class="section-heading">
                        <h5 class="__subtitle">Shots of our mining centers</h5>
                        <h2 class="__title">Our Mining Centers</h2>
                     </div>
                  </div>
                  <div class="col hide--md col-MB-10"></div>
                  <div class="col col--md-7 col--lg-8">
                     <p>
                        Our Mining centers are spread across the world in all continets.
                     </p>

                     </p>
                  </div>
               </div>
            </div>
            <div class="col-MB-50"></div>
            <!-- start gallery -->
            <div class="gallery">
               <div class="__inner">
                  <div class="row">
                     <!-- start item -->
                     <div class="col col--sm-6 col--md-5">
                        <div class="__item" data-y="2" ddata-aos="fade-up" ddata-aos-delay="100" ddata-aos-offset="200">
                           <figure class="__image">
                              <img class="lazy" src="/landing/img/blank.gif" data-src="/landing/img/gallery_img/1.jpg" alt="demo" />
                           </figure>
                           <div class="__content">
                              <p>Modern Equipment</p>
                              <a class="__link" href="/landing/img/gallery_img/1.jpg" data-fancybox="gallery"></a>
                           </div>
                        </div>
                     </div>
                     <!-- end item -->
                     <!-- start item -->
                     <div class="col col--sm-6 col--md-7">
                        <div class="__item" data-y="2" ddata-aos="fade-up" ddata-aos-delay="100" ddata-aos-offset="200">
                           <figure class="__image">
                              <img class="lazy" src="/landing/img/blank.gif" data-src="/landing/img/gallery_img/2.jpg" alt="demo" />
                           </figure>
                           <div class="__content">
                              <p>Modern Equipment</p>
                              <a class="__link" href="/landing/img/gallery_img/2.jpg" data-fancybox="gallery"></a>
                           </div>
                        </div>
                     </div>
                     <!-- end item -->
                     <!-- start item -->
                     <div class="col col--sm-6 col--lg-5">
                        <div class="__item" data-y="1" ddata-aos="fade-up" ddata-aos-delay="100" ddata-aos-offset="200">
                           <figure class="__image">
                              <img class="lazy" src="/landing/img/blank.gif" data-src="/landing/img/gallery_img/3.jpg" alt="demo" />
                           </figure>
                           <div class="__content">
                              <p>Modern Equipment</p>
                              <a class="__link" href="/landing/img/gallery_img/3.jpg" data-fancybox="gallery"></a>
                           </div>
                        </div>
                     </div>
                     <!-- end item -->
                     <!-- start item -->
                     <div class="col col--sm-6 col--lg-4">
                        <div class="__item" data-y="1" ddata-aos="fade-up" ddata-aos-delay="100" ddata-aos-offset="200">
                           <figure class="__image">
                              <img class="lazy" src="/landing/img/blank.gif" data-src="/landing/img/gallery_img/4.jpg" alt="demo" />
                           </figure>
                           <div class="__content">
                              <p>Modern Equipment</p>
                              <a class="__link" href="/landing/img/gallery_img/4.jpg" data-fancybox="gallery"></a>
                           </div>
                        </div>
                     </div>
                     <!-- end item -->
                     <!-- start item -->
                     <div class="col col--sm-6 col--lg-3">
                        <div class="__item" data-y="1" ddata-aos="fade-up" ddata-aos-delay="100" ddata-aos-offset="200">
                           <figure class="__image">
                              <img class="lazy" src="/landing/img/blank.gif" data-src="/landing/img/gallery_img/5.jpg" alt="demo" />
                           </figure>
                           <div class="__content">
                              <p>Modern Equipment</p>
                              <a class="__link" href="/landing/img/gallery_img/5.jpg" data-fancybox="gallery"></a>
                           </div>
                        </div>
                     </div>
                     <!-- end item -->
                  </div>
               </div>
            </div>
            <!-- end gallery -->
         </section>
         <!-- end section -->
         <!-- start section -->
         <section class="section section--dark-bg">
            <div class="grid grid--container">
               <div class="section-heading section-heading--white section-heading--center  col-MB-60">
                  <h5 class="__subtitle">We are worldwide</h5>
                  <h2 class="__title">We have mining farms all over the world</h2>
               </div>
               <img class="lazy img-responsive center-block" src="/landing/img/blank.gif" data-src="/landing/img/world_map.png" alt="demo" />
            </div>
         </section>
         <!-- end section -->
         <!-- start section -->
         <section class="section">
            <div class="grid grid--container">
               <div class="section-heading section-heading--center  col-MB-60">
                  <h5 class="__subtitle">FAQ</h5>
                  <h2 class="__title">Have any questions?</h2>
               </div>
               <div class="row row--xs-middle">
                  <div class="col col--md-10">
                     <!-- start FAQ -->
                     <div class="faq">
                        <div class="accordion-container">
                           <!-- start item -->
                           <div class="accordion-item">
                              <div class="accordion-toggler">
                                 <h4 class="__title h5">Who can mine?</h4>
                                 <i class="circled"></i>
                              </div>
                              <article>
                                 <div class="__inner">
                                    <p>
                                      Anyone with a phone or computer can start utilising our platform and earn with crypto mining
                                    </p>
                                 </div>
                              </article>
                           </div>
                           <!-- end item -->
                           <!-- start item -->
                           <div class="accordion-item">
                              <div class="accordion-toggler">
                                 <h4 class="__title h5">Where are Miningstorm mining centers?</h4>
                                 <i class="circled"></i>
                              </div>
                              <article>
                                 <div class="__inner">
                                    <p>
                                       Miningstorm has centers throughout the whole world and in all continets
                                    </p>

                                 </div>
                              </article>
                           </div>
                           <!-- end item -->
                           <!-- start item -->
                           <div class="accordion-item">
                              <div class="accordion-toggler">
                                 <h4 class="__title h5">What is the minimum amount to start with?</h4>
                                 <i class="circled"></i>
                              </div>
                              <article>
                                 <div class="__inner">
                                    <p>
                                       You can start with as little as $10
                                    </p>

                                 </div>
                              </article>
                           </div>
                           <!-- end item -->
                           <!-- start item -->
                           <div class="accordion-item">
                              <div class="accordion-toggler">
                                 <h4 class="__title h5">How does one deposit?</h4>
                                 <i class="circled"></i>
                              </div>
                              <article>
                                 <div class="__inner">
                                    <p>
                                      Miningstorm deposits are automatic and very fast to approve.
                                    </p>
                                    <p>
                                      All you need to do is login and click on deposit on your dashboard, select the method you want to deposit with that is available in your region.
                                    </p>
                                 </div>
                              </article>
                           </div>
                           <!-- end item -->
                           <!-- start item -->
                           <div class="accordion-item">
                              <div class="accordion-toggler">
                                 <h4 class="__title h5">How does one withdraw?</h4>
                                 <i class="circled"></i>
                              </div>
                              <article>
                                 <div class="__inner">
                                    <p>
                                      Once on your dashboard, you need to add a withdrawal account and to withdraw you just click on withdraw and enter your amount.
                                    </p>

                                 </div>
                              </article>
                           </div>
                           <!-- end item -->

                        </div>

                     </div>
                     <!-- end FAQ -->
                  </div>
               </div>
            </div>
         </section>
         <!-- end section -->



      </main>
      <!-- end main -->
      <!-- start footer -->
      <footer id="footer" class="text--center text--lg-left">
         <div class="grid grid--container">
            <div class="row row--xs-middle">
               <div class="col col--sm-10 col--md-8 col--lg-3">
                  <div class="__item">
                     <a class="site-logo" href="{{url('/')}}" style="margin: 0 0 20px;">
                     <img class="img-responsive lazy" width="175" height="42" src="/landing/img/blank.gif" data-src="/images/logo-dark.png" alt="demo" />
                     </a>
                     <div class="social-btns">
                        <a class="fontello-twitter" href="#"></a>
                        <a class="fontello-facebook" href="#"></a>
                        <a class="fontello-linkedin-squared" href="#"></a>
                     </div>
                     <span class="__copy">
                     © 2022, Mining Storm <a class="__dev" href="#" target="_blank"></a> | <a href="#">Privacy&nbsp;Policy</a> | <a href="#">Sitemap</a>
                     </span>
                  </div>
               </div>
               <div class="col col--sm-10 col--md-8 col--lg-4">
                  <div class="__item">
                     <h4 class="__title">Contact Information</h4>
                     <address class="__text">
                      <br>
                        Whatsapp: <a href="tel:+1(066)05239876">(066) 052 39876</a><br>
                        Email: <a href="mailto:info@miningstorm.co.za">info@miningstorm.co.za</a>
                     </address>
                  </div>
               </div>
               <div class="col col--sm-10 col--md-8 col--lg-5">
                  <div class="__item">
                     <h4 class="__title">Main menu</h4>
                     <nav id="footer__navigation" class="navigation">
                        <div class="row row--xs-middle">
                           <div class="col col--xs-auto col--md-3 col--lg-4">
                              <ul class="__menu">
                                 <li><a href="{{url('/')}}">Home</a></li>
                                 <li><a href="{{url('/')}}">About us</a></li>

                              </ul>
                           </div>
                           <div class="col col--xs-auto col--md-3 col--lg-4">
                              <ul class="__menu">
                                 <li><a href="{{url('/')}}">Dashboard</a></li>
                                 <li><a href="{{url('/')}}">Login</a></li>

                              </ul>
                           </div>

                        </div>
                     </nav>
                  </div>
               </div>
            </div>
         </div>
      </footer>
      <!-- end footer -->
      <div id="btn-to-top-wrap">
         <a id="btn-to-top" class="circled" href="javascript:void(0);" data-visible-offset="800"></a>
      </div>
      <!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/623fc9760bfe3f4a876fe073/1fv4ilv4o';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
      <script src="/ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
      <script>window.jQuery || document.write('<script src="/landing/js/jquery-2.2.4.min.js"><\/script>')</script>
      <script type="text/javascript" src="/landing/js/main.min.js"></script>
      <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
      <script>
         (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
         function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
         e=o.createElement(i);r=o.getElementsByTagName(i)[0];
         e.src='/www.google-analytics.com/analytics.js';
         r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
         ga('create','UA-XXXXX-X','auto');ga('send','pageview');
      </script>
   </body>
   <!-- Mirrored from demo.artureanec.com/html/crypterium/index_8.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 10 Mar 2022 08:28:58 GMT -->
</html>
